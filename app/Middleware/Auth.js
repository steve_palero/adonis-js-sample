/* eslint-disable consistent-return */
/* eslint-disable no-cond-assign */
// /app/Middleware/Auth.js

'use strict'

const Jwt = use('Jwt')
const User = use('App/Models/User')
const Token = use('App/Models/Token')

class Auth {
  async handle (ctx, next) {
    const { request, response } = ctx

    let token
    if ((token = request.header('authorization'))) {
      token = token.split(' ')
      token = token.length === 2 && token[1]
    }

    if (!token) {
      return response.unauthorized({
        status: 401,
        message: 'No authorization token found'
      })
    }

    const data = Jwt.verify(token)
    if (!data) {
      return response.unauthorized({
        status: 401,
        message: 'Provided token is not valid'
      })
    }
    const verifyToken = await Token.findOne({ token })
    if (verifyToken) {
      const user = await User.findOne({
        _id: data.user,
        deletedAt: null
      }).populate('roles')

      if (!user) {
        return response.unauthorized({
          status: 401,
          message: 'User not found'
        })
      }
      ctx.auth = { user, token }

      await next()
    } else {
      return response.unauthorized({
        status: 401,
        message: 'Token has expired.'
      })
    }
  }
}

module.exports = Auth
