'use strict'

class v1VerifyEmail {
  get validateAll () {
    return true
  }

  get rules () {
    return {
      email: 'required',
      code: 'required'
    }
  }

  get messages () {
    return {
      'email.required': 'Email is required.',
      'code.required': 'Code is required.'
    }
  }

  async fails (errorMessages) {
    // console.log('errors', errorMessages)
    return this.ctx.response.status(400).send({
      status: 400,
      message: 'Validation failed.',
      errors: errorMessages
    })
  }
}

module.exports = v1VerifyEmail
