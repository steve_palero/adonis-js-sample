'use strict'

class v1StoreBlog {
  get validateAll () {
    return true
  }

  get rules () {
    return {
      title: 'required',
      description: 'required'
    }
  }

  get messages () {
    return {
      'title.required': 'Title is required.',
      'description.required': 'Description is required.'
    }
  }

  async fails (errorMessages) {
    // console.log('errors', errorMessages)
    return this.ctx.response.status(400).send({
      status: 400,
      message: 'Validation failed.',
      errors: errorMessages
    })
  }
}

module.exports = v1StoreBlog
