'use strict'

class v1RegisterUser {
  get validateAll () {
    return true
  }

  get rules () {
    return {
      email: 'required|email',
      password: 'required',
      firstName: 'required',
      lastName: 'required'
    }
  }

  get messages () {
    return {
      'email.required': 'Email is required.',
      'email.email': 'Email given is not valid.',
      'firstName.required': 'First name is required.',
      'lastName.required': 'Last name is required.'
    }
  }

  async fails (errorMessages) {
    // console.log('errors', errorMessages)
    return this.ctx.response.status(400).send({
      status: 400,
      message: 'Validation failed.',
      errors: errorMessages
    })
  }
}

module.exports = v1RegisterUser
