'use strict'

class v1ResendCode {
  get validateAll () {
    return true
  }

  get rules () {
    return { email: 'required' }
  }

  get messages () {
    return { 'email.required': 'Email is required.' }
  }

  async fails (errorMessages) {
    // console.log('errors', errorMessages)
    return this.ctx.response.status(400).send({
      status: 400,
      message: 'Validation failed.',
      errors: errorMessages
    })
  }
}

module.exports = v1ResendCode
