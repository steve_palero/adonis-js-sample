'use strict'

const moment = require('moment')

const Queue = use('Queue')

const Blog = use('App/Models/Blog')

class BlogController {
  async get ({ response }) {
    const blogs = await Blog.find({})
    return response.ok({
      status: 200,
      message: 'Success',
      data: { blogs }
    })
  }

  async store ({ request, response, auth }) {
    const {
      title,
      description
    } = request.post()

    const slug = title.replace(/\s+/g, '-').toLowerCase()
    const toCreateSlug = `${slug}${moment().valueOf()}`
    try {
      await Blog.create({
        slug: toCreateSlug,
        title,
        description,
        author: { name: auth.user.name }
      })
      await Queue.add({
        name: 'EmailQueue',
        payload: {
          to: auth.user.email,
          message: `You have published a blog. - '${title}'`,
          subject: 'Blog published'
        }
      }, { delay: 10 * 1000 })
      return response.created({
        status: 201,
        message: 'Success',
        data: {
          blog: {
            slug: toCreateSlug,
            title,
            description,
            author: { name: auth.user.name }
          }
        }
      })
    } catch (e) {
      return response.internalServerError({
        status: 500,
        message: e
      })
    }
  }

  async getBlog ({ params, response }) {
    const { slug } = params
    const blog = await Blog.findOne({ slug })
    if (blog) {
      return response.ok({
        status: 200,
        message: 'Success',
        data: { blog }
      })
    }
    return response.notFound({
      status: 404,
      message: 'Blog not found.'
    })
  }

  async update ({ params, request, response }) {
    const blog = await Blog.findOneAndUpdate({ slug: params.slug }, {
      title: request.input('title'),
      description: request.input('description')
    })
    if (blog !== null) {
      return response.ok({
        status: 200,
        message: 'Success',
        data: blog
      }, { new: true })
    }
    return response.notFound({
      status: 404,
      message: 'Blog not found'
    })
  }

  async delete ({ params, response }) {
    const { slug } = params
    const blogList = await Blog.find({})
    if (blogList.length > 0) {
      await Blog.deleteOne({ slug })
      return response.ok({
        status: 200,
        message: 'Blog successfully deleted.'
      })
    }
    return response.notFound({
      status: 404,
      message: 'Blog not found.'
    })
  }
}

module.exports = BlogController
