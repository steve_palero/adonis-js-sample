const jwt = require('jsonwebtoken')

const Hash = use('Hash')
const Queue = use('Queue')

const User = use('App/Models/User')
const Token = use('App/Models/Token')

class AuthController {
  async login ({ request, response }) {
    const {
      email,
      password
    } = request.post()

    const user = await User.findOne(
      { email, deletedAt: null },
      '+password'
    ).lean()

    if (!user) {
      return response.notFound({
        status: 404,
        message: 'Invalid Username or Password'
      })
    }

    const isSame = await Hash.verify(password, user.password)

    if (!isSame) {
      return response.conflict({
        status: 409,
        message: 'Invalid Username or Password'
      })
    }

    if (user.isVerified) {
      const token = jwt.sign({ user: String(user._id) }, process.env.APP_KEY, { expiresIn: '5d' })

      await Token.create({ token })

      return response.ok({
        status: 200,
        message: 'Login Successful',
        data: token
      })
    }
    return response.unauthorized({
      status: 401,
      message: 'Email not yet verified.'
    })
  }

  async logout ({ request, response }) {
    let token
    const header = request.header('authorization')
    token = header.split(' ')
    token = token.length === 2 && token[1]

    const removeToken = await Token.deleteOne({ token })
    if (removeToken.ok === 1 && removeToken.deletedCount > 0) {
      return response.ok({
        status: 200,
        message: 'Success'
      })
    }
    return response.notFound({
      status: 404,
      message: 'Provided token is not valid'
    })
  }

  async register ({ request, response }) {
    const { email, firstName, lastName, password } = request.post()

    try {
      const code = Math.floor(100000 + Math.random() * 900000)
      await User.create({
        email,
        isVerified: false,
        verificationCode: code,
        name: { firstName, lastName },
        password: await Hash.make(password)
      })

      await Queue.add({
        name: 'EmailQueue',
        payload: {
          to: email,
          message: `Your confirmation code is ${code}`,
          subject: 'Email verification'
        }
      }, { delay: 10 * 1000 })

      return response.created({
        status: 201,
        message: 'Registration Successful'
      })
    } catch (error) {
      return response.internalServerError({
        status: 500,
        message: 'Something went wrong'
      })
    }
  }

  async verifyEmail ({ request, response }) {
    const { email, code } = request.post()
    try {
      const user = await User.findOne({ email, deletedAt: null })
      if (!user) {
        return response.notFound({
          status: 404,
          message: 'User not found.'
        })
      }
      if (!user.isVerified) {
        if (user.verificationCode === code) {
          const updateUser = await User.findOneAndUpdate({ email }, { isVerified: true })
          if (updateUser !== null) {
            await Queue.add({
              name: 'EmailQueue',
              payload: {
                to: email,
                message: 'Your have successfuly verified your account.',
                subject: 'Email Verified!'
              }
            }, { delay: 10 * 1000 })
            return response.ok({
              status: 200,
              message: 'Email verified.'
            })
          }
          return response.unprocessableEntity({
            status: 422,
            message: 'Failed to save data'
          })
        }
        return response.unprocessableEntity({
          status: 422,
          message: 'Invalid code.'
        })
      }
      return response.conflict({
        status: 409,
        message: 'Email already verified.'
      })
    } catch (e) {
      return response.internalServerError({
        status: 500,
        message: 'Something went wrong'
      })
    }
  }

  async resendCode ({ request, response }) {
    const { email } = request.post()
    try {
      const user = await User.findOne({ email })
      if (!user) {
        return response.notFound({
          status: 404,
          message: 'User not found.'
        })
      }

      if (!user.isVerified) {
        const code = Math.floor(100000 + Math.random() * 900000)
        const updateUser = await User.findOneAndUpdate({ email }, { verificationCode: code })
        if (updateUser !== null) {
          await Queue.add({
            name: 'EmailQueue',
            payload: {
              to: email,
              message: `Your confirmation code is ${code}`,
              subject: 'Email verification'
            }
          }, { delay: 10 * 1000 })
          return response.ok({
            status: 200,
            message: 'Code has been sent to your email.'
          })
        }
        return response.unprocessableEntity({
          status: 422,
          message: 'Failed to send code to your email.'
        })
      }

      return response.conflict({
        status: 409,
        message: 'Email already verified.'
      })
    } catch (e) {
      return response.internalServerError({
        status: 500,
        message: 'Something went wrong'
      })
    }
  }

  async user ({ response, auth }) {
    return response.status(200).send({
      status: 200,
      message: 'Success',
      data: auth.user
    })
  }
}

module.exports = AuthController
