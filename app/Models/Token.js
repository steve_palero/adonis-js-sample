'use strict'

const mongoose = use('Providers/Mongoose')

const Token = mongoose.model('Token', { token: { type: String, unique: true } })

module.exports = Token
