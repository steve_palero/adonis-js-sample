'use strict'

const mongoose = use('Providers/Mongoose')

const Blog = mongoose.model('Blog',
  {
    slug: { type: String, unique: true },
    title: { type: String },
    author: { type: Object },
    description: { type: String }
  })

module.exports = Blog
