// /app/Models/User.js

'use strict'

const mongoose = use('Providers/Mongoose')
const { Schema } = use('Providers/Mongoose')

const UserSchema = new Schema({
  email: {
    type: String,
    index: true,
    unique: true
  },
  isVerified: { type: Boolean },
  verificationCode: { type: String },
  password: {
    type: String,
    required: true,
    select: false
  },
  name: {
    firstName: {
      type: String,
      required: true,
      uppercase: true
    },
    lastName: {
      type: String,
      required: true,
      uppercase: true
    }
  },
  deletedAt: {
    type: Date,
    default: null
  },
  createdAt: {
    type: Date,
    default: Date.now,
    required: true
  }
})

// Indexes...
UserSchema.index({
  'name.firstName': 'text',
  'name.lastName': 'text'
})

module.exports = mongoose.model('User', UserSchema)
