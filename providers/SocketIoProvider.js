// /providers/SocketIoProvider.js

'use strict'

const { ServiceProvider } = require('@adonisjs/fold')

class SocketIoProvider extends ServiceProvider {
  /**
   * Register namespaces to the IoC container
   *
   * @method register
   *
   * @return {void}
   */
  register () {
    this.app.singleton('Providers/SocketIo', app => {
      const options = {}
      const io = require('socket.io')(app.use('Server').getInstance(), options)

      io.on('connection', socket => {
        socket.on('disconnect', () => {})

        socket.on('chat message', msg => {
          io.emit('chat message', msg)
        })
      })

      return io
    })
    //
  }

  /**
   * Attach context getter when all providers have
   * been registered
   *
   * @method boot
   *
   * @return {void}
   */
  boot () {
    this.app.use('Providers/SocketIo')
  }
}

module.exports = SocketIoProvider
