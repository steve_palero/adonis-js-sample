// /providers/QueueProvider.js

'use strict'

const { ServiceProvider } = require('@adonisjs/fold')

const Queue = require('bull')

class QueueProvider extends ServiceProvider {
  register () {
    this.app.singleton('Providers/QueueProvider', () => new Queue('App', 'redis://127.0.0.1:6379', { prefix: 'hlcynnjs101' }))

    this.app.alias('Providers/QueueProvider', 'Queue')
  }

  boot () {
    const Queuee = this.app.use('Queue')
    const Mail = this.app.use('Mail')

    Queuee.process(async (job, done) => {
      const { name, payload } = job.data

      if (name === 'EmailQueue') {
        const { to, message, subject } = payload
        await Mail.raw(message, messages => {
          messages.from(process.env.EMAIL_SENDER)
          messages.subject(subject)
          messages.to(to)
        })

        done()
      }
    })
  }
}

module.exports = QueueProvider
