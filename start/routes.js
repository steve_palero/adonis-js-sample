'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
const Env = use('Env')
const Config = use('Config')
const swaggerJSDoc = use('swagger-jsdoc')

Route.group(() => {
  Route.post('register', 'v1/AuthController.register').validator('v1/RegisterUser')
  Route.post('verify-email', 'v1/AuthController.verifyEmail').validator('v1/VerifyEmail')
  Route.post('resend-code', 'v1/AuthController.resendCode').validator('v1/ResendCode')
  Route.post('login', 'v1/AuthController.login').validator('v1/LoginUser')
  Route.delete('logout', 'v1/AuthController.logout').middleware('auth')
  Route.get('user', 'v1/AuthController.user').middleware('auth')
}).prefix('v1/auth')

Route.group(() => {
  Route.get('blogs', 'v1/BlogController.get')
  Route.get('blogs/:slug', 'v1/BlogController.getBlog')
  Route.post('blogs', 'v1/BlogController.store').validator('v1/StoreBlog').middleware('auth')
  Route.put('blogs/:slug', 'v1/BlogController.update').validator('v1/StoreBlog').middleware('auth')
  Route.delete('blogs/:slug', 'v1/BlogController.delete').middleware('auth')
}).prefix('v1')

Route.get('/chat', ({ view }) => view.render('chat'))

// Swagger Routes
Route.get('/api-specification', async () => {
  // https://swagger.io/docs/specification/about/
  const swaggerDefinition = {
    openapi: '3.0.0',
    info: {
      title: Config.get('app.name'),
      version: '1.0.0'
    },
    servers: [{ url: '/v1' }],
    basePath: '/v1',
    security: [{ bearerAuth: [] }],
    schemes: Env.get('NODE_ENV') === 'production' ? ['https'] : ['http'],
    components: {
      securitySchemes: {
        bearerAuth: {
          type: 'http',
          scheme: 'bearer',
          bearerFormat: 'JWT'
        }
      }
    }
  }

  const options = {
    definition: swaggerDefinition,
    apis: ['./app/**/**.js', './app/**/**.yaml']
  }

  return swaggerJSDoc(options)
})

Route.get('/api-documentation', ({ view }) => view.render('swagger', { specUrl: '/api-specification' }))
