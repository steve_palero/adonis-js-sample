'use strict'

// start/app.js

const path = require('path')

/*
|--------------------------------------------------------------------------
| Providers
|--------------------------------------------------------------------------
|
| Providers are building blocks for your Adonis app. Anytime you install
| a new Adonis specific package, chances are you will register the
| provider here.
|
*/
const providers = [
  '@adonisjs/framework/providers/AppProvider',
  '@adonisjs/bodyparser/providers/BodyParserProvider',
  '@adonisjs/validator/providers/ValidatorProvider',
  '@adonisjs/cors/providers/CorsProvider',
  '@adonisjs/websocket/providers/WsProvider',
  '@adonisjs/framework/providers/ViewProvider',
  '@adonisjs/mail/providers/MailProvider',

  path.join(__dirname, '..', 'providers', 'MongooseProvider'),
  path.join(__dirname, '..', 'providers', 'JwtProvider'),
  path.join(__dirname, '..', 'providers', 'SocketIoProvider'),
  path.join(__dirname, '..', 'providers', 'QueueProvider')
]

/*
|--------------------------------------------------------------------------
| Ace Providers
|--------------------------------------------------------------------------
|
| Ace providers are required only when running ace commands. For example
| Providers for migrations, tests etc.
|
*/
const aceProviders = [
]

/*
|--------------------------------------------------------------------------
| Aliases
|--------------------------------------------------------------------------
|
| Aliases are short unique names for IoC container bindings. You are free
| to create your own aliases.
|
| For example:
|   { Route: 'Adonis/Src/Route' }
|
*/
const aliases = {}

/*
|--------------------------------------------------------------------------
| Commands
|--------------------------------------------------------------------------
|
| Here you store ace commands for your package
|
*/
const commands = []

module.exports = { providers, aceProviders, aliases, commands }
